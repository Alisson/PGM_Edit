#ifndef _IMAGEMPGM_HPP_
#define _IMAGEMPGM_HPP_

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>


#define Tam_Resposta        17 // Tamanho do array para respostas
#define Tam_Comentario      257 // Tamanho do array para inser��o de coment�rios no arquivo

using namespace std;

class imagemPgm{
    public:
        imagemPgm();
        ~imagemPgm();
        bool carregaImagem(string Nome); // Carregamento de imagem
        bool salvaImagem(string Nome); // Salvamento de imagem
        bool aplicaNegativo(); // Invers�o de imagem
        bool aplicaSharpen(); // Aplica��o de filtro sharpen
        bool aplicaSmooth(); // Aplica��o de filtro smooth

    private:
        // m�todos
        bool aplicaFiltro(short* Filtro, short div=1); // Aplica��o de filtro dados matriz-filtro e divisor (1 por default)
        int resgataNumero(FILE* arquivo);

        // atributos
        short* mapaPixels; // "matriz" de pixels da imagem
        int Width; // largura da imagem
        int Height; // altura da imagem
        int MaxCinza; // n�vel m�ximo de cinza

};


#endif // _IMAGEMPGM_HPP_
