SRCFOLDER := src/
INCFOLDER := inc/
OBJFOLDER := obj/
BINFOLDER := bin/
PROG_NAME := PGM_Edit

CC := g++
CFLAGS := -Wall -ansi

SRCFILES := $(wildcard src/*.cpp)
# acessa o conteudo de src/

all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CFLAGS) "$(OBJFOLDER)*.o" -o "$(BINFOLDER)$(PROG_NAME)"

obj/%.o: src/%.cpp
	$(CC) $(CFLAGS) -c "$<" -o "$@" -I./inc
# $< usa src/%.cpp (primeira dependencia)
# $@ usa obj/%.o (nome do alvo)


.PHONY: cls
# O alvo cls � tomado como uma "funcao" e pode ser chamado por: make clean

cls:
	rm -rf $(OBJFOLDER)*
	rm -rf $(BINFOLDER)*
