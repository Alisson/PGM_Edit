#include "imagemPgm.hpp"


int main() {

    // INICIALIZAÇÃO
    imagemPgm* foto = new imagemPgm;
    if(foto==NULL) {
        cerr << " Nao foi possivel inicializar corretamente!" << endl;
    } else {
        string NomeArquivo;
        string Comando;
        // ABERTURA DE CONTATO COM USUARIO
        do { // loop de execução

            // Impressao de parametros iniciais
            cout << "\n\tPGM_Edit" << endl
            << "\n Insira o comando referente a opcao desejada: " << endl
            << " A - Abrir Imagem" << endl << " S - Sair" << endl
            << " >";
            getline(cin, Comando);
            if(Comando=="A" || Comando=="a") {
                // MANIPULAÇÃO DE IMAGEM

                do {
                        cout << "\n Insira o caminho e o nome da imagem que deseja abrir:\n >";
                        getline(cin, NomeArquivo);
                        cout << endl;
                        if(foto->carregaImagem(NomeArquivo)) { // imagem carregada com sucesso

                            do {
                                cout << " Insira o comando referente a opcao desejada: " << endl
                                << " A - Salvar Imagem" << endl << " B - Aplicar Negativo" << endl
                                << " C - Aplicar filtro Sharpen" << endl << " D - Aplicar filtro Smooth" << endl
                                << " V - Voltar ao Menu Principal" << endl << " S - Sair do Programa (sem salvar)" << endl << " >";
                                getline(cin, Comando);
                                cout << endl;
                                if(Comando=="V" || Comando=="v") break; // voltar ao menu inicial
                                if(Comando=="A" || Comando=="a") { // comandos A, B, C e D
                                    // salvar imagem
                                    cout << " Insira o caminho e o nome da imagem que deseja salvar:\n >";
                                    getline(cin, NomeArquivo);
                                    if(foto->salvaImagem(NomeArquivo)) cout << "\n Imagem salva com sucesso";
                                    else cerr << " Nao foi possivel salvar a imagem";
                                    cout << "!\n\n";
                                } else if(Comando=="B" || Comando=="b") {
                                    // aplicar negativo
                                    if(foto->aplicaNegativo()) cout << " Negativo aplicado com sucesso";
                                    else cerr << " Nao foi possivel aplicar o negativo";
                                    cout << "!\n\n";
                                } else if(Comando=="C" || Comando=="c") {
                                    // aplicar sharpen
                                    if(foto->aplicaSharpen()) cout << " Filtro aplicado com sucesso";
                                    else cerr << " Nao foi possivel aplicar o filtro";
                                    cout << "!\n\n";
                                } else if(Comando=="D" || Comando=="d") {
                                    // aplicar smooth
                                    if(foto->aplicaSmooth()) cout << " Filtro aplicado com sucesso";
                                    else cerr << " Nao foi possivel aplicar o filtro";
                                    cout << "!\n\n";
                                } else if(Comando != "S" && Comando != "s") {
                                    cout << " Comando Invalido!\n\n";
                                }

                            } while(Comando != "S" && Comando != "s");

                            break;
                        }

                        // ERRO AO TENTAR ABRIR
                        cout << " Nao foi possivel abrir a imagem!" << endl
                        << "\n Deseja tentar novamente?" << endl;
                        do {
                            cout << " (S / N) >";
                            getline(cin, NomeArquivo);
                        } while(NomeArquivo != "S" && NomeArquivo != "s" && NomeArquivo != "N" && NomeArquivo != "n");
                } while(NomeArquivo == "S" || NomeArquivo == "s");

            } else if(Comando!="S" && Comando!="s") {
                cout << " Comando Invalido!" << endl;
            }
        } while(Comando != "S" && Comando != "s");

        // FINALIZAÇÃO
        delete foto;
    }

    return 0;
}


