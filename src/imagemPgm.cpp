#include "imagemPgm.hpp"


imagemPgm::imagemPgm() { // atributos iniciam zerados (facilita a verifica��o de algum erro)
    mapaPixels = NULL;
    Width = Height = MaxCinza = 0;
}

imagemPgm::~imagemPgm() {
    if(mapaPixels != NULL) delete []mapaPixels;
    mapaPixels = NULL;
}

bool imagemPgm::carregaImagem(string Nome) {
/* Leitura geral do arquivo de imagem e verifica��o de erros
*
*   Depois de muito tempo tentando fazer funcionar com ifstreams conclui que � muito mais
*   f�cil utilizar as bibliotecas do pr�prio C (pelo menos pra mim):
*
*       1 - Verifica se o arquivo existe e pode ser aberto
*       2 - Verifica se ja existe uma imagem carregada e a deleta (se existir)
*       3 - Verifica a presen�a do "n�mero m�gico"
*       4 - Ignora todas as linhas iniciadas por # (apenas at� a pr�xima etapa)
*       5 - L� os atributos de largura, altura e maxCinza (respectivamente)
*       6 - Aloca espa�o para a matriz de pixels e a constroi
*       7 - Fecha o arquivo e retorna true;
*       # Qualquer erro: retorna false (depois de liberar a mem�ria alocada)
*/

    char * chamada = new char[Nome.size() + 1];
    if(chamada==NULL) return false; // falha na aloca��o
    copy(Nome.begin(), Nome.end(), chamada);
    chamada[Nome.size()] = '\0';

	/* etapa 1 Abrir arquivo */
	FILE* arquivo = fopen(chamada, "r+b");
	delete []chamada;
	if(arquivo==NULL) return false;

	/*variaveis auxiliares*/
	char c; // auxiliar de leitura
	int i; // auxiliar de lacos

	/* etapa 2 - Numero magico */
	i = 0;
	fread(&c, 1, 1, arquivo);
	if(c == 'P') i++;
	fread(&c, 1, 1, arquivo);
	if(c == '5') i++;
	if(i != 2) {
		// Numero magico nao lido corretamente (ou nao existente)
		fclose(arquivo);
		return false;
	}

	/* etapa 3 - Limpeza de imagem pre-existente */
	if(mapaPixels != NULL) {
		free(mapaPixels);
		mapaPixels = NULL;
	}

	/* etapa 4 - Carregar atributos */
	Width = resgataNumero(arquivo);
	Height = resgataNumero(arquivo);
	MaxCinza = resgataNumero(arquivo);

	/* etapa 5 - Alocacao de matriz de pixels */
	mapaPixels = (short*)calloc(Width*Height, sizeof(short));
	if(mapaPixels==NULL) {
		// falha na alocacao
		fclose(arquivo);
		return false;
	}

	/* etapa 6 - Preenchimento da matriz e finalizacao */
	for(i=0; i<Width*Height; i++) { // percorrer matriz
			fread(&mapaPixels[i], 1, 1, arquivo);
	}
	fclose(arquivo);
	return true;
}

bool imagemPgm::salvaImagem(string Nome) {
/* Salva a imagem dado o nome do arquivo (juntamente com a extens�o)
*       1 - Verifica se o arquivo pode ser aberto para escrita (caso esteja protegido, resultar� em erro)
*       2 - Grava o "n�mero m�gico"
*       3 - Solicita coment�rios (tantos quantos desejados)
*       4 - Grava os atributos de largura, altura e maxCinza (respectivamente)
*       5 - Grava a matriz de pixels
*       6 - Fecha o arquivo e retorna true;
*       # Qualquer erro: retorna false (depois de liberar a mem�ria alocada)
*/
    if(mapaPixels==NULL) return false; // n�o existe imagem carregada (corretamente pelo menos)
    char* chamada = new char[Nome.size() + 1];
    if(chamada==NULL) return false; // falha na aloca��o
    copy(Nome.begin(), Nome.end(), chamada);
    chamada[Nome.size()] = '\0';

    // Auxiliares para registro de coment�rios
    char* comentario = new char[Tam_Comentario];
    if(comentario==NULL) { // falha na aloca��o
        delete []chamada;
        return false;
    }
    char* resposta = new char[Tam_Resposta];
    if(resposta==NULL) { // falha na aloca��o
        delete []chamada;
        delete []comentario;
        return false;
    }

    FILE* arquivo = fopen(chamada, "w+b");
    delete []chamada; // libera��o de mem�ria usada para nome do arquivo
    if(arquivo==NULL) return false; // erro na abertura do arquivo

    // GRAVAR N�MERO M�GICO
    fputs("P5\n", arquivo);

    // LOOP DE COMENT�RIOS
    do { // recebe comentarios que ser�o inseridos no arquivo
        cout << "\n Deseja Inserir um comentario? (S - Sim / N - Nao): ";
        cin.getline(resposta, Tam_Resposta);

        if(strcmp(resposta, "N") && strcmp(resposta, "n") && strcmp(resposta, "S") && strcmp(resposta, "s"))
            cout << " Comando invalido!\n Digite \'S\' para Sim ou \'N\' para Nao\n "; // valida��o
        else if(!strcmp(resposta, "S") || !strcmp(resposta, "s")) {
            cout << "\n Insira seu comentario:\n ";
            cin.getline(comentario, Tam_Comentario);
            fputs("# ", arquivo);
            fputs(comentario, arquivo);
            fputs("\n", arquivo);
        }

    } while(strcmp(resposta, "N") && strcmp(resposta, "n"));

    // GRAVA��O DOS ATRIBUTOS
    fprintf(arquivo, "%d %d\n%d\n", Width, Height, MaxCinza);

    // GRAVA��O DA MATRIZ DE PIXELS
    for(int i=0; i<Width*Height; i++) fwrite(&mapaPixels[i], 1, 1, arquivo);

    fclose(arquivo);

    return true;
}

bool imagemPgm::aplicaNegativo() {
    if(mapaPixels==NULL) return false; // n�o existe imagem carregada (corretamente pelo menos)
    for(int i=0; i<Width*Height; i++) mapaPixels[i] = MaxCinza - mapaPixels[i];
    return true;
}

bool imagemPgm::aplicaSharpen() {
    if(mapaPixels==NULL) return false; // n�o existe imagem carregada (corretamente pelo menos)

    short* filtro = new short[9];
    if(filtro==NULL) return false; // falha na aloca��o

    // montagem do filtro
    filtro[0] = filtro[2] = filtro[6] = filtro[8] = 0;
    filtro[1] = filtro[3] = filtro[5] = filtro[7] = -1;
    filtro[4] = 5;

    bool teste = aplicaFiltro(filtro, 1);
    delete []filtro;

    return teste;
}

bool imagemPgm::aplicaSmooth() {
    if(mapaPixels==NULL) return false; // n�o existe imagem carregada (corretamente pelo menos)
    short* filtro = new short[9];
    if(filtro==NULL) return false; // falha na aloca��o

    // montagem do filtro
    for(int i=0; i<9; i++) filtro[i] = 1;

    bool teste = aplicaFiltro(filtro, 9);
    delete []filtro;

    return teste;
}

bool imagemPgm::aplicaFiltro(short* Filtro, short div) {
    short* copia = new short[(Width-2)*(Height-2)];
    if(copia==NULL) return false; // falha na aloca��o

    int i, j, k, l;
    short val;

    for(i=1; i<Height-1; i++) { // percorrer linhas (menos a margem)
        for(j=1; j<Width-1; j++) { // percorrer colunas (menos a margem)
            val = 0;
            for(k=-1; k<=1; k++) {
                for(l=-1; l<=1; l++) {
                    val += Filtro[3*(k+1) + l + 1] * mapaPixels[(i+k)*Width + (j+l)];
                }
            }

            val /= div;
            if(val > MaxCinza) val = MaxCinza;
            else if(val < 0) val = 0;

            copia[(i-1)*(Width-1) + j -1] = (short)val;
        }
    }

    for(i=1; i<Height-1; i++) { // percorrer linhas (menos a margem)
        for(j=1; j<Width-1; j++) { // percorrer colunas (menos a margem)
                mapaPixels[i*Width + j] = copia[(i-1)*(Width-1) + j -1];
        }
    }

    delete []copia;

    return true;
}

int imagemPgm::resgataNumero(FILE* arquivo) {
// Leitura do arquivo e busca por um numero em decimal

    if(arquivo==NULL) return 0; // nao ha arquivo

    char c; // auxiliar de leitura
    int valor = 0; // auxiliar de retorno (valor inicial crucial para correto funcionamento)

    fread(&c, 1, 1, arquivo);
    do { // loop de procura de caractere chave

            if(c=='#') do { // loop de leitura e impressao na tela dos comentarios
                cout << c;
                fread(&c, 1, 1, arquivo);
                if(c=='\n') cout << c;
            } while(c!='\n' && !feof(arquivo));

            if(c>='0' && c<='9') do { // loop de gravacao de numero
                valor *= 10;
                valor += (unsigned int)(c-'0');
                fread(&c, 1, 1, arquivo);
            } while(c>='0' && c<='9' && !feof(arquivo));

            if(valor==0) fread(&c, 1, 1, arquivo); // leitura de proximo caractere caso o numero ainda seja nulo

    } while(valor==0 && !feof(arquivo));
    // feof so se valida ao ler o ultimo caractere pela segunda vez (com fread)

    return valor;
}
